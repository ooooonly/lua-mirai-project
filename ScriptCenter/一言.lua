-- LuaMiraiScript --
-- name: 一言
-- author: Mayuri
-- version: 0.1
-- description: 一言
-- /LuaMiraiScript --
print("载入bot成功")
Event.subscribe("FriendMessageEvent", function(event)
    if event.message == ("一言") then
        event.sender:sendMessage(Quote(event.message) +
                                     "你要的一言来了:\n" ..
                                     Http.get(
                                         "https://api.ixiaowai.cn/ylapi/index.php"))
    end
end)
Event.subscribe("GroupMessageEvent", function(event)
    if event.message == ("一言") then
        event.group:sendMessage(Quote(event.message) +
                                    "你要的一言来了:\n" ..
                                    Http.get(
                                        "https://api.ixiaowai.cn/ylapi/index.php"))
    end
end)

Event.onFinish = function() print("脚本被卸载！") end
