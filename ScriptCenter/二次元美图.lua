-- LuaMiraiScript --
-- name: 来点二次元美图
-- author: Mayuri
-- version: 0.1
-- description: 一个能获取二次元美图并往群里发送的脚本
-- /LuaMiraiScript --
print("载入二次元美图成功")

Event.subscribe("GroupMessageEvent", function(event)
    if tostring(event.message):find("来点二次元") then
        local erciyuan = "https://api.ixiaowai.cn/api/api.php"
        event.group:sendMessage(ImageUrl(erciyuan, event.group))
    end
end)

Event.subscribe("FriendMessageEvent", function(event)
    if tostring(event.message):find("来点二次元") then
        local erciyuan = "https://api.ixiaowai.cn/api/api.php"
        event.friend:sendMessage(ImageUrl(erciyuan, event.friend))
    end
end)

Event.onFinish = function() print("脚本被卸载！") end