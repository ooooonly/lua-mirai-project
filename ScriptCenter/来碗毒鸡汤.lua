-- LuaMiraiScript --
-- name: 来碗毒鸡汤
-- author: jiasswee
-- version: 1.0
-- description: 试着向机器人发送‘来碗毒鸡汤’
-- /LuaMiraiScript --
Event.subscribe("GroupMessageEvent", function(event)
    if tostring(event.message):find("毒鸡汤") then
        event.group:sendMessage(Quote(event.message) ..
                              Http.get("https://du.shadiao.app/api.php"))
    end
end)

Event.subscribe("FriendMessageEvent", function(event)
    if tostring(event.message):find("毒鸡汤") then
        event.friend:sendMessage(Quote(event.message) ..
                              Http.get("https://du.shadiao.app/api.php"))
    end
end)