-- LuaMiraiScript --
-- name: 另一个一言
-- author: Mayuri
-- version: 0.2
-- description: 一个简单的一言
-- /LuaMiraiScript --
Event.subscribe("FriendMessageEvent", function(event)
    if tostring(event.message):find("一言") then
        source = Http.get("https://v1.hitokoto.cn"):match([["from":"(.-)","]])
        author =
            Http.get("https://v1.hitokoto.cn"):match([["creator":"(.-)","]])
        yiyan = Http.get("https://v1.hitokoto.cn"):match(
                    [["hitokoto":"(.-)","type"]])
        event.sender:sendMessage(Quote(event.message) .. yiyan .. "\n来源:" ..
                                     source .. "\n作者:" .. author)
    end
end)
Event.subscribe("GroupMessageEvent", function(event)
    if tostring(event.message):find("一言") then
        source = Http.get("https://v1.hitokoto.cn"):match([["from":"(.-)","]])
        author =
            Http.get("https://v1.hitokoto.cn"):match([["creator":"(.-)","]])
        yiyan = Http.get("https://v1.hitokoto.cn"):match(
                    [["hitokoto":"(.-)","type"]])
        event.group:sendMessage(Quote(event.message) .. yiyan .. "\n来源:" ..
                                    source .. "\n作者:" .. author)
    end
end)

Event.onFinish = function() print("脚本被卸载！") end
