-- LuaMiraiScript --
-- name: 来点壁纸
-- author: Mayuri
-- version: 0.1
-- description: 一个能获取壁纸并往群里发送的脚本
-- /LuaMiraiScript --
print("载入壁纸获取成功")
Event.subscribe("GroupMessageEvent", function(event)
    if tostring(event.message):find("来点壁纸") then
        local bz = "https://api.ixiaowai.cn/gqapi/gqapi.php"
        event.group:sendMessage(ImageUrl(bz, event.group))
    end
end)

Event.onFinish = function() print("脚本被卸载！") end
