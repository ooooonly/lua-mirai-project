-- LuaMiraiScript --
-- name: 来点menhera
-- author: Mayuri
-- version: 0.1
-- description: 一个能获取menhera并往群里发送的脚本
-- /LuaMiraiScript --
print("载入menhera图获取成功")
Event.subscribe("GroupMessageEvent", function(event)
    if tostring(event.message):find("来点menhera") then
        local menhera = "https://api.ixiaowai.cn/mcapi/mcapi.php"
        event.group:sendMessage(ImageUrl(menhera, event.group))
    end
end)

Event.onFinish = function() print("脚本被卸载！") end
