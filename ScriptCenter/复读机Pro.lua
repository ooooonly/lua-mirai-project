-- LuaMiraiScript --
-- name: 复读机pro
-- author: ooooonly
-- version: 1.0
-- description: 机器人会按照概率随机复读
-- /LuaMiraiScript --
-- 按概率随机复读

Event.subscribe("GroupMessageEvent", function(event)
    if math.random(1, 10) == 1 then event.group:sendMessage(event.message) end
end)
