-- LuaMiraiScript --
-- name: 网抑云
-- author: wyl
-- version: 0.1
-- description: 一个简单的网抑云语录
-- /LuaMiraiScript --
print("载入网抑云语录成功")
Event.subscribe("FriendMessageEvent", function(event)
    if tostring(event.message):find("来点网抑云") then
        api_json = Http.get("http://api.heerdev.top:4995/nemusic/random")
        api_message = api_json:match([["text":"(.-)"}]])
        event.sender:sendMessage(Quote(event.message) .. api_message)
    end
end)
Event.subscribe("GroupMessageEvent", function(event)
    if event.message:tostring():match("来点网抑云") then
        api_json = Http.get("http://api.heerdev.top:4995/nemusic/random")
        api_message = api_json:match([["text":"(.-)"}]])
        event.group:sendMessage(Quote(event.message) .. api_message)
    end
end)

Event.onFinish = function() print("脚本被卸载！") end
