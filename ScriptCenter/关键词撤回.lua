-- LuaMiraiScript --
-- name: 关键词撤回
-- author: ooooonly
-- version: 1.0
-- description: 默认撤回关键词为‘撤回’
-- /LuaMiraiScript --
Event.subscribe("GroupMessageEvent", function(event)
    if tostring(event.message):find("撤回") then -- 可自定义关键词
        event.message:recall()
    end
end)

