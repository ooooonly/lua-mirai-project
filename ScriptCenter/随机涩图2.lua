-- LuaMiraiScript --
-- name: 随机涩图2
-- author: ooooonly
-- version: 1.0
-- description: 试着向机器人发送‘色图’
-- /LuaMiraiScript --
Event.subscribe("GroupMessageEvent", function(event)
    if tostring(event.message):find("hso") or tostring(event.message):find("色图") or
        tostring(event.message):find("不够色") then
        event.group:snedImage(http.getRedirectUrl(
                                "https://api.r10086.com/%E5%8A%A8%E6%BC%AB%E7%BB%BC%E5%90%881.php",
                                "https://img.r10086.com/"))
    end
end)