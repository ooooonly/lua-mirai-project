-- LuaMiraiScript --
-- name: 关键词回复pro
-- author: ooooonly
-- version: 1.0
-- description: 可自定义回复内容
-- /LuaMiraiScript --
-- 支持使用lua模式匹配
local responses = {
    ["夸我"] = function(message)
        return Http.get(Quote(message) .. "https://chp.shadiao.app/api.php")
    end,
    ["骂我"] = function(message)
        return Quote(message) +
                   Http.get(
                       "https://nmsl.shadiao.app/api.php?level=min&lang=zh_cn")
    end
}

local function checkResponse(message)
    for k, v in pairs(responses) do
        if (message:find(k)) then return v(message) end
    end
    return nil
end

Event.subscribe("GroupMessageEvent", function(event)
    local resp = checkResponse(event.message)
    if resp then event.group:sendMessage(resp) end
end)
