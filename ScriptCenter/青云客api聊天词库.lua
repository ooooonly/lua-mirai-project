-- LuaMiraiScript --
-- name: 青云客聊天
-- author: Mayuri
-- version: 0.1
-- description: 一个简单的聊天bot
-- /LuaMiraiScript --
print("载入青云客聊天bot成功")
Event.subscribe("FriendMessageEvent", function(event)
    if tostring(event.message):find("##") then
        api_value = tostring(event.message):gsub("##", "")
        api_json = Http.get(
                       "http://api.qingyunke.com/api.php?key=free&appid=0&event.message=" ..
                           api_value)
        api_message = api_json:gsub("{\"result\":0,\"content\":\"", ""):gsub(
                          "\"}", ""):gsub("{br}", "\n"):gsub(
                          "{\"result\":1,\"content\":\"", ""):gsub("菲菲",
                                                                   "cqname")
                          :gsub("你特么滚！我老公是tianyu",
                                "该词条已删除")
        event.sender:sendMessage(Quote(event.message) .. api_message)
    end
end)
Event.subscribe("GroupMessageEvent", function(event)
    if tostring(event.message):find("##") then
        api_value = tostring(event.message):gsub("##", "")
        api_json = Http.get(
                       "http://api.qingyunke.com/api.php?key=free&appid=0&event.message=" ..
                           api_value)
        api_message = api_json:gsub("{\"result\":0,\"content\":\"", ""):gsub(
                          "\"}", ""):gsub("{br}", "\n"):gsub(
                          "{\"result\":1,\"content\":\"", ""):gsub("菲菲",
                                                                   "cqname")
                          :gsub("你特么滚！我老公是tianyu",
                                "该词条已删除")
        event.group:sendMessage(Quote(event.message) + api_message)
    end
end)

Event.onFinish = function() print("脚本被卸载！") end