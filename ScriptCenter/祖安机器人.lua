-- LuaMiraiScript --
-- name: 祖安机器人
-- author: ooooonly
-- version: 1.0
-- description: 试着向机器人发送‘骂我’
-- /LuaMiraiScript --
Event.subscribe("GroupMessageEvent", function(event)
    if event.message == "骂我" then
        event.group:sendMessage(Quote(event.message) + At(event.sender) +
                                    http.get(
                                        "https://nmsl.shadiao.app/api.php?level=min&lang=zh_cn"))

    elseif tostring(event.message):find("骂他") then
        local at
        for k, v in pairs(event.message:toTable()) do
            if v:find("mirai:at") then
                at = v
                break
            end
        end
        event.group:sendMessage(at ..
                                    http.get(
                                        "https://nmsl.shadiao.app/api.php?lang=zh_cn"))
    end
end)