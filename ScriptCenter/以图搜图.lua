-- LuaMiraiScript --
-- name: 以图搜图
-- author: chorblack
-- version: 1.0
-- description: 试着向机器人发送以图搜图帮助
-- /LuaMiraiScript --
print("载入以图搜图bot成功")
_apikey = "" -- 请在""中输入apikey
Event.subscribe("FriendMessageEvent", function(event)
    if tostring(event.message):find("以图搜图帮助") then
        event.sender:sendMessage(
            "以图搜图apikey请去https://saucenao.com/user.php?page=search-api申请，额度一天300\n指令：#以图搜图直接加图片，暂时不能分离")
    end
end)

Event.subscribe("GroupMessageEvent", function(event)
    if tostring(event.message):find("#以图搜图") then
        event.group:sendMessage("开始搜索，请稍等")
        for i, m in ipairs(event.message:toTable()) do
            if (m:find("mirai:image")) then
                x = m:getImageUrl()
                api_url =
                    ("https://saucenao.com/search.php?db=999&output_type=2&testmode=1&numres=16&api_key=" ..
                        _apikey .. "&url=" .. x)
                local count = 0
                while true do
                    count = count + 1
                    json, isSuccessful, code = Http.get(api_url)
                    if code == 200 then break end
                    if count > 5 then
                        event.group:sendMessage("网络错误")
                        break
                    end

                end

                if json:find(
                    "Specified file no longer exists on the remote server!") then
                    event.group:sendMessage(
                        "插件或网络错误，请重启试试")
                end
                json = json:gsub("%[", "")
                json = json:gsub("%]", "")
                url = json:match('"ext_urls":"(.-)"')
                url = url:gsub("\\", "")
                illust_id = url:match('id=(%d+)')
                member_id = json:match('"member_id":(%d+)')
                if illust_id == nil then
                    event.group:sendMessage(
                        "未搜到p站图片，正在启用ascii2d搜索")
                    local urls = {}
                    api_json = ("https://ascii2d.net/search/url/" .. x)
                    local num = 0
                    while true do
                        num = num + 1
                        html, isSuccessful, code = Http.get(api_json)
                        if code == 200 then break end
                        if num > 5 then
                            event.group:sendMessage("网络错误")
                            break
                        end
                    end
                    pic_ids = {}
                    for l in html:gmatch(
                                 '<a target="_blank" rel="noopener" href="(.-)"') do
                        table.insert(pic_ids, l)
                    end
                    for i in html:gmatch('<img loading="lazy" src="(.-)"') do
                        url = ('https://ascii2d.net' .. i)
                        table.insert(urls, url)
                    end
                    local x = 0
                    local id_num = -1
                    for j, k in pairs(urls) do
                        x = x + 1
                        id_num = id_num + 2
                        event.group:sendMessage(
                            ImageUrl(k, event.group) .. pic_ids[id_num])
                        if x > 3 then break end
                    end

                else
                    local urls = {}
                    api_json =
                        ("https://api.imjad.cn/pixiv/v2/?type=illust&id=" ..
                            illust_id)
                    local number = 0
                    while true do
                        number = number + 1
                        html, isSuccessful, code = Http.get(api_json)
                        if code == 200 then break end
                        if number > 5 then
                            event.group:sendMessage("网络错误")
                            break
                        end
                    end
                    if html:find("error") then
                        event.group:sendMessage(
                            "引擎错误,正在启用ascii2d搜索")

                        api_json = ("https://ascii2d.net/search/url/" .. x)
                        while true do
                            html, isSuccessful, code = Http.get(api_json)
                            if code == 200 then break end
                        end
                        by_id = html:match(
                                    '<a target="_blank" rel="noopener" href="(.-)"')
                        by = html:match('<img loading="lazy" src="(.-)"')
                        by_url = ('https://ascii2d.net' .. by)
                        event.group:sendMessage(
                            ImageUrl(by_url, event.group) .. by_id)

                    end

                    for i in html:gmatch('"original_image_url":"(.-)"') do
                        url1 = i:gsub("\\", "")
                        url = url1:gsub("i.pximg.net", "i.pixiv.cat")
                        table.insert(urls, url)
                    end
                    local x = 0
                    for j, k in pairs(urls) do
                        x = x + 1
                        event.group:sendMessage(
                            ImageUrl(k, event.group) .. k .. "\n" .. "插画id=" ..
                                illust_id .. "\n" .. "画师id=" .. member_id)
                        if x > 5 then break end
                    end

                end

            end
        end
    end
end)

Event.onFinish = function() print("脚本被卸载！") end
