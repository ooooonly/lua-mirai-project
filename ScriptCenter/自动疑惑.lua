-- LuaMiraiScript --
-- name: 疑惑机
-- author: Mayuri
-- version: 1.0
-- description: 自动疑惑
-- /LuaMiraiScript --
Event.subscribe("GroupMessageEvent", function(event)
    if event.message == ("?") or event.message == ("？") or tostring(event.message):find("¿") then
        Amaze =
            ("https://3k8hzvh3r37by.cfc-execute.bj.baidubce.com/Online%20Picture/Auto%20Amazed.jpg")
        -- Amaze = ("http://gchat.qpic.cn/gchatpic_new/195439320/1058878835-2209040529-C3E3218AC337A3071F1B9D1249BECAD0/0?term=2")
        event.group:sendMessage(ImageUrl(Amaze, event.group))
    end
end)

Event.onFinish = function() print("脚本被卸载！") end
