-- LuaMiraiScript --
-- name: 整点报时
-- author: ooooonly
-- version: 1.0
-- description: 整点报时
-- /LuaMiraiScript --
local isReport
local bot = Bot(123456)
Event.subscribe("GroupMessageEvent", function(event)
    if event.message == "开启整点报时" then
        thread(function()
            isReport = true
            local remainMinutes = 60 - tonumber(os.date("%M", os.time()))
            sleep(remainMinutes * 60 * 1000)
            while isReport do
                bot.getGroup(123456):sendMessage(
                    "整点报时，现在是：" ..
                        tonumber(os.date("%H", os.time())) .. "时0分")
                sleep(60 * 60 * 1000) -- 延迟1小时
            end
        end)
    elseif message == "关闭整点报时" then
        isReport = false
    end
end)
