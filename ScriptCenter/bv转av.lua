-- LuaMiraiScript --
-- name: B站bv转av的bot
-- author: Mayuri
-- version: 0.1
-- description: 一个简单bv转av event.bot
-- /LuaMiraiScript --
Event.subscribe("FriendMessageEvent",
    function(event)
        if tostring(event.message):find("bv转av") or tostring(event.message):find("AV转BV") then
            event.sender:sendMessage("输入\"获取av号\"+BV(bv)+BV号即可获取AV号")
        end

        if tostring(event.message):find("获取av号") then
            api_value = tostring(event.message):gsub("获取av号","")
            api_json = Http.get("https://api.tzg6.com/api/bilibv?bv=" .. api_value)
            api_message = api_json:gsub([[{"status":"ok","av":"]],""):gsub("\"}",""):gsub([[{"status":"error","av":"Decoding failed.]],"获取AV号失败")
            event.sender:sendMessage(Quote(event.message)+api_message)
        end
    end
)
Event.subscribe("GroupMessageEvent",
    function(event)		
        if tostring(event.message):find("bv转av") or tostring(event.message):find("AV转BV") then
            event.group:sendMessage("输入\"获取av号\"+BV(bv)+BV号即可获取AV号")
        end
        
    	if tostring(event.message):find("获取av号") then
            api_value = tostring(event.message):gsub("获取av号","")
            api_json = Http.get("https://api.tzg6.com/api/bilibv?bv=" .. api_value)
            api_message = api_json:gsub([[{"status":"ok","av":"]],""):gsub("\"}",""):gsub([[{"status":"error","av":"Decoding failed.]],"获取AV号失败")
            event.group:sendMessage(Quote(event.message)+api_message)
        end
    end
)


Event.onFinish = 
function ()
print("脚本被卸载！")
end