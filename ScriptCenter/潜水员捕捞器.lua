-- LuaMiraiScript --
-- name: 戳泡机&潜水员捕捞器
-- author: Mayuri
-- version: 0.2
-- description: 一个能快速戳泡&捞起潜水员的bot
-- /LuaMiraiScript --
print("载入戳泡&捞潜水员bot成功")
Event.subscribe("GroupMessageEvent", function(event)
    if not tostring(event.message):find("的是") and tostring(event.message):find("冒") and
        tostring(event.message):find("泡") then
        event.group:sendMessage(Quote(event.message) .. "戳掉~~")
    end

    if not tostring(event.message):find("的是") and tostring(event.message):find("潜水") or
        tostring(event.message):find("下潜") then
        event.group:sendMessage(Quote(event.message) + "捞起来~~")
    end
end)

Event.onFinish = function() print("脚本被卸载！") end
