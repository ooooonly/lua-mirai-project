-- LuaMiraiScript --
-- name: 复读机
-- author: ooooonly
-- version: 1.0
-- description: 复读机
-- /LuaMiraiScript --
Event.subscribe("FriendMessageEvent",
                function(event) event.friend:sendMessage(event.message) end)

Event.subscribe("GroupMessageEvent",
                function(event) event.group:sendMessage(event.message) end)
