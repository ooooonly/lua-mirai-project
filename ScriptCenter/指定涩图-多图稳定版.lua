-- LuaMiraiScript --
-- name: 指定色图-多图稳定版
-- author: chorblack
-- version: 1.4
-- description: 试着向机器人发送涩图帮助
-- /LuaMiraiScript --
print("载入涩图bot成功")
_apikey = "" -- 请在""中输入apikey
-- 私聊
r18 = 0
master = {} -- 请改为你的QQ，多管理员用,隔开

function isInArray(t, val)
    for _, v in ipairs(t) do if v == val then return true end end
    return false
end

Event.subscribe("FriendMessageEvent", function(event)
    if tostring(event.message):find("涩图帮助") then
        event.sender:sendMessage(
            "apikey的申请需要用到的工具:tg，条件：科学上网\n来源：https://moe.best/cross.html\napikey申请地址：https://t.me/loliconApiBot\n_18为r18的控制开关，填写0为无r18，1为全r18，2为混合")
        event.sender:sendMessage(
            "指令合集：#st+关键词+数量，也可以直接#st+数量得到随机涩图\n例如：#stfgo 3，#st 4，最后的数量要用空格隔开\n还有一种情况，例如#st德莉莎等不加数量时，不要加空格\nr18开启指令：私聊：开启/关闭r18，需要管理员权限，群聊：#开启/关闭r18")
        event.sender:sendMessage(
            "另：不能使用（主要是timeout）时，科学上网\n无apikey时一天只有几次测试机会\n请填写apikey与master，添加了网络不好时的无图模式(#开启/关闭无图模式)")

    end

    if tostring(event.message):find("开启r18") then
        local TF = isInArray(master, event.sender.id)

        if TF == true then
            r18 = 1
            event.sender:sendMessage("r18开启成功")
        else
            event.sender:sendMessage("无权限")
        end
    end

    if tostring(event.message):find("关闭r18") then
        r18 = 0
        event.group:sendMessage("r18已关闭")
    end
    if tostring(event.message):find("#st") then
        event.sender:sendMessage("start")
        api_value = tostring(event.message):gsub("#st", "")
        if api_value:find(" ", -3) == nil then
            api_value = api_value .. " 1"
        end
        api_num = api_value:match("%d", -2)
        api_value = api_value:sub(1, -3)
        if api_value == nil then api_value = '' end
        if api_num == nil then api_num = 1 end
        api_json = ("https://api.lolicon.app/setu/?apikey=" .. _apikey ..
                       "&r18=" .. r18 .. "&num=" .. api_num .. "keyword=" ..
                       api_value)
        html = Http.get(api_json)

        if html:find("没有符合条件的色图") then
            event.sender:sendMessage("没有符合条件的色图")
        end

        for i in html:gmatch('"url":"(.-)"') do
            url = i:gsub("\\", "")
            event.sender:sendMessage(ImageUrl(url, event.sender))
        end
    end
end)
-- 群聊
_r18 = 0
switch = 1
Event.subscribe("GroupMessageEvent", function(event)
    if tostring(event.message):find("#开启r18") then
        local TF = isInArray(master, event.sender.id)
        if TF == true then
            _r18 = 1
            event.group:sendMessage("r18开启成功")
        else
            event.group:sendMessage("无权限")
        end
    end

    if tostring(event.message):find("#关闭r18") then
        _r18 = 0
        event.group:sendMessage("r18已关闭")
    end

    if tostring(event.message):find("#开启无图模式") then
        local TF = isInArray(master, event.sender.id)
        if TF == true then
            switch = 0
            event.group:sendMessage("无图模式打开")
        else
            event.group:sendMessage("无权限")
        end
    end

    if tostring(event.message):find("#关闭无图模式") then
        switch = 1
        event.group:sendMessage("无图模式关闭")
    end

    if tostring(event.message):find("#st") then
        event.group:sendMessage(ImageUrl("/8A93FC90-BB57-E1D5-20B3-00FA487AB9DE",
                                      event.group))
        api_value = tostring(event.message):gsub("#st", "")
        if api_value:find(" ", -3) == nil then
            api_value = api_value .. " 1"
        end
        api_num = api_value:match("%d", -2)
        api_value = api_value:sub(1, -3)
        if api_value == nil then api_value = '' end
        if api_num == nil then api_num = 1 end
        api_value = api_value:gsub(" ", "")
        api_json = ("https://api.lolicon.app/setu/?apikey=" .. _apikey ..
                       "&r18=" .. _r18 .. "&keyword=" .. api_value .. "&num=" ..
                       api_num)
        while true do
            html, isSuccessful, code = Http.get(api_json)
            if isSuccessful == true then
                event.group:sendMessage("准备完毕，请稍等")
                break
            else
                event.group:sendMessage("网络错误……")
                break
            end
        end

        print("code=" .. code)
        print(html)
        if html:find("没有符合条件的色图") then
            event.group:sendMessage("没有符合条件的色图")
        end

        if switch == 1 then
            for i in html:gmatch('"url":"(.-)"') do
                url = i:gsub("\\", "")
                event.group:sendMessage(ImageUrl(url, event.group)) -- 一次发一张
            end
        else
            for i in html:gmatch('"url":"(.-)"') do
                url = i:gsub("\\", "")
                event.group:sendMessage(url)
                sleep(3000)
            end
        end
    end
end)

Event.onFinish = function() print("脚本被卸载！") end
